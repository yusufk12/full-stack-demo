from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField

class HotelSearchForm(FlaskForm):
    city = StringField('City')
    checkin = StringField('Checkin')
    checkout = StringField('Checkout')
    submit = SubmitField('Submit')