# Fullstack Demo
This is our task for potential new fullstack engineers. We expect this task to take less than two hours of work

Please fork this repo and make a starting commit with the message 'start'.
Commit your finished code and push to your forked repo when you are finished. Thanks 😄

## TODO

Implement a simple flask web app that serves html.

If you are unfamiliar with flask the documentation can be found here

https://flask.palletsprojects.com/en/1.1.x/#user-s-guide

but the flask boilerplate is provided to you in this demo. Please add your service code in the @app.route('/') endpoint in app.py. The HTML should be added to templates/demo.html.

Any new dependencies should be added to requirements.txt

To run the service please cd into your local repo directory and run:
```
pip install -r requirements.txt
export FLASK_APP=app.py
python -m flask run
```

This solution uses Redis to cache endpoint responses (justification as to why this was chosen will be in the module docstring).

To use the Redis cache, open a new terminal window, and follow this tutorial to configure Redis: https://realpython.com/python-redis/

Once the shell command `redis-server` has been run, you're good to go.

Once this is done, you'll have a Redis server initialized, and the API responses will be able to be cached and retrieved properly!

Note: if the redis server times out and you want to re-boot, sometimes it freezes in which case:
`ps aux | grep redis-server` 
to see all running instances, and Then kill:
`kill -9 22292`

The service will be running at http://127.0.0.1:5000/.


# NOTE:
I had some access denied errors due to my git config, so I had to refork on different gitlab account!
Original repo was here:
https://gitlab.com/yusuf-khaled/full-stack-demo
And I started at 10:48 AM there, and finished here at 12:47 PM 
