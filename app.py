"""
This is a Flask web app that retrieves hotels based on user input.

Because the HTML was so basic, the main consideration here was the caching mechanism, as we were told the endpoint was expensive to call.

There are multiple different possibilities for caching, such as: Database, Redis, in-memory cache.

There are drawbacks to each method.

The business use case should be considered in order to come to the right decision.

Some questions to think about:
  Is it that much faster to hit cache?
  How often do we have to invalidate it?
  Will it interact with other things that complicate invalidation? 
  --> In this case, our API endpoint could be updated every minute with entirely new results! That would mean our cache would be invalidated every minute!
  Is it a local or remote cache?
  Is it shared between users/sites?

Redis/DB are nice because they can be scaled horizontally: if multiple web servers are spun up, they can all access the same cache data
However these solutions are more technically expensive to access data, as they're further away than in-memory cache, for example.

It's also dependent on the nature of the data. If you're accessing live sports scores from an NBA game via web socket, caching is less viable, as the data you receive
will be invalidated so often, due to how quickly the NBA game changes. 
In other applications, the cache will be invalidated much slower!

In this case, I've chosen a Redis cache as it's a nice, lightweight cache with easy-to-set expiry values and key stores.
Imagining a scenario where this app is served over multiple servers, Redis allows us a nice universally consistent cache.

And, because the cached data is stored at a key value that's deterministic (will be the exact same for every identical input) every server will have the exact same 
data retrieved for each identical input.
"""
from flask import Flask, render_template, flash
import requests
import redis
import pickle
from form import HotelSearchForm
app = Flask(__name__)

app.config.update(
    WTF_CSRF_ENABLED=True,
    WTF_CSRF_TIME_LIMIT=None,
    SECRET_KEY='snaptravel-fullstackdemo-flask-secret'
)

API_URL = 'https://experimentation.snaptravel.com/interview/hotels'

# Set cached responses to expire in 10 minutes (business use case decision - perhaps this needs to be changed depending on when the info returned is refreshed)
CACHE_EXPIRY_TIME = 60*10

@app.route('/', methods=['GET', 'POST'])
def hello_world():
    """
    Main method
    """
    form = HotelSearchForm()
    results = {}
    num_results = 0
    redis_conn = get_redis_conn()
    # Once the form is submitted properly, get data either via cache or by querying API
    if form.validate_on_submit():
        results, num_results = query(form)
        cache_key = get_cache_key(form)  # Deterministically create a key based on the form input to store the API request
        cached_results = redis_conn.get(cache_key)  # Check if there are cached requests for this input - will return None if expired/non-existent
        if cached_results is not None:
            results = pickle.loads(cached_results)  # Set our results to the cached value -- must be deserialized using pickle
            num_results = len(results)
            flash(f'🎉 Found ({num_results}) great option(s) in cache! 🎉', 'success')
        else:
            results, num_results = query(form)
            if num_results > 0:
                redis_conn.set(cache_key, pickle.dumps(results), ex=CACHE_EXPIRY_TIME)  # Redis doesn't accept object inputs --> serialize then store as a string      
                flash(f'🎉 Found ({num_results}) great option(s)! 🎉', 'success')
            else:
                flash(f'Unfortunately we got no results returned for this search. Try again!', 'error')

    return render_template('demo.html', name='world', form=form, results=results, num_results=num_results)

def get_redis_conn():
    """
    Establish connection with redis
    """
    r = redis.StrictRedis(host='localhost', port=6379, db=0)
    return r

def get_cache_key(form):
    """
    Based on the form input, create a deterministic key to store the cached responses
    """
    return '_'.join([str(form.city), str(form.checkin), str(form.checkout)])

def query(form):
    """
    Call the api and get hotels
    """
    # Get api results
    snap_payload = {'city': form.city, 'checkin': form.checkin, 'checkout': form.checkout, 'provider': 'snaptravel'}
    retail_payload = {'city': form.city, 'checkin': form.checkin, 'checkout': form.checkout, 'provider': 'retail'}
    snap_results = requests.post(API_URL, data=snap_payload).json()
    retail_results = requests.post(API_URL, data=retail_payload).json()
    # Find common ids between retail/snap results
    s_ids = [snap_val.get("id") for snap_val in snap_results.get('hotels')]
    r_ids = [retail_val.get("id") for retail_val in retail_results.get('hotels')]
    common_hotel_ids = [val for val in s_ids if val in r_ids]
    result = []
    # Amalgamate common id values with snap and retail prices attached
    for hotel_id in common_hotel_ids:
        snap_data = [s_val for s_val in snap_results.get('hotels') if s_val.get("id") == hotel_id][0]
        retail_data = [r_val for r_val in retail_results.get('hotels') if r_val.get("id") == hotel_id][0]
        if "snap_price" not in snap_data:
            snap_data["snap_price"] = snap_data.pop("price")
        if "retail_price" not in retail_data:
            snap_data["retail_price"] = retail_data["price"]
        result.append(snap_data)    
    return result, len(result)